<?php

// Some global variables
$mailmanArchives = '/var/lib/mailman/archives/public/';
$marcUrlBase = 'http://marc.info';
$kdeArchiveUrlBase = 'https://mail.kde.org/pipermail';
$kdeArchiveListPage = 'https://mail.kde.org/mailman/listinfo/';

// Some general variables, preinitialized to empty
$timePeriod = null;
$listname = null;
$redirectUrl = null;

// Do we need to try to translate a time period parameter?
if( isset($_GET['b']) ) {
	// Try to separate the year and month
	$actualDate = date_create_from_format('Yn', $_GET['b']);
	// Now put it into Mailman style...
	$timePeriod = date_format( $actualDate, 'Y-F' );
}

// Do we have a listname?
if( isset($_GET['l']) ) {
	// Transfer it
	$listname = $_GET['l'];

	// Where inside the archives should we be looking for this list + time period?
	$archivesPath = null;
	if( $timePeriod !== null ) {
		$archivesPath = sprintf('%s/%s/', $listname, $timePeriod);
	} else { 
		$archivesPath = sprintf('%s/', $listname);
	}

	// Where will that path be on disk?
	$localDiskArchivesPath = sprintf('%s/%s', $mailmanArchives, $archivesPath);

	// Do the archives exist?
	if( file_exists($localDiskArchivesPath) ) {
		// Create the redirect url
		$redirectUrl = sprintf('%s/%s', $kdeArchiveUrlBase, $archivesPath);
	}
}

// If we have no query parameters, we will handle the redirect ourselves
if( $_SERVER['QUERY_STRING'] === '' ) {
	// Set the redirect URL appropriately
	$redirectUrl = $kdeArchiveListPage;
}

// If the Message or Thread ID variables are set we cannot handle this, so pass it on straight to MARC
// Also, if no redirect url is set, then forward to MARC
if( isset($_GET['m']) || isset($_GET['t']) || $redirectUrl === null ) {
	// Redirect to MARC
	$redirectUrl = sprintf('%s/?%s', $marcUrlBase, $_SERVER['QUERY_STRING']);
}

// Perform the redirect
header("Location: $redirectUrl", true, 301);

